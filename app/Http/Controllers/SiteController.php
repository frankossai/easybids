<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    //
    public function index()
    {
        $products = Product::orderBy('updated_at', 'desc')->simplePaginate(10);
        return view('landing',  ['products' => $products]);
    }
}
