<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class ProductController extends Controller
{

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $products = Product::orderBy('updated_at', 'desc')->get();
        return view('product/list',  ['products' => $products]);
    }
    /**
     * Show the form for creating a Product.
     *
     * @return Response
     */
    public function create()
    {
        return view('product/create');
    }

    /**
     * Create a new flight instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        try {
            $this->validate($request, [
                'name' => 'required|max:500',
                'sku' => 'required|unique:products|max:55',
                'description' => 'required',
                'price' => 'required|numeric',
            ]);

            $product = new Product();

            $product->name = $request->name;
            $product->sku = $request->sku;
            $product->price = $request->price;
            $product->description = $request->description;

            if ($product->save())
            {
                Session::flash('message', 'Product created successfully!');
                return redirect()->action('DashboardController@index');
            }
        }
        catch (Exception $e)
        {
            Session::flash('message', $e->getMessage());
        }
    }


    /**
     * Update the specified Product in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $product = Product::find($id);
        if(!$product)
        {
            Session::flash('message', 'Product not found');
            return redirect()->action('DashboardController@index');
        }
        return view('product/edit',  ['product' => $product]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $this->validate($request, [
            'name' => 'required|max:500',
            'sku' => 'required|unique:products,sku,'.$product->id.'|max:55',
            'description' => 'required',
            'price' => 'required|numeric',
        ]);


        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->price = $request->price;
        $product->description = $request->description;

        if ($product->save()) {
            Session::flash('message', 'Product saved successfully!');
            return redirect()->action('DashboardController@index');
        }

    }

    /**
     * Delete the specified Product
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $product = Product::find($id);
        if($product->delete())
        {
            Session::flash('message', 'Product deleted successfully!');
        }
        return redirect()->action('DashboardController@index');
    }

}
