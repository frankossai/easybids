<?php

namespace App;

trait ProductLog
{
    protected static function boot()
    {
        parent::boot();
        Product::created(function ($product)
        {
            self::saveLog($product, 'created');
        });

        Product::deleted(function ($product)
        {
            self::saveLog($product, 'deleted');
        });

        Product::updated(function ($product)
        {
            self::saveLog($product, 'updated');
        });
    }

    protected static function saveLog($product, $action)
    {
        $log = New Log();
        $log->product_id = $product->id;
        $log->user_id = \Auth::id();
        $log->type = $action;
        $log->save();
    }
}