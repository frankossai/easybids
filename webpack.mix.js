let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .combine([
        'resources/assets/js/bootbox.min.js',
        'resources/assets/js/holder.min.js',
        'resources/assets/js/jquery.matchHeight.js',
        'resources/assets/js/bid-app.js'
    ],  'public/js/plugins.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/web.scss', 'public/css');
