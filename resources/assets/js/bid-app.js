$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    //delete-item
    $(document).on('click', '.delete-item', function () {
        var $btn = $(this);
        var product_name = $btn.data('name');
        var action_url = $btn.data('url');
        bootbox.confirm({
            message: "You are about to delete " + product_name,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result === true) {
                    jQuery(location).attr('href', action_url);
                }
            }
        });
    });
    var $msgAlert = $('.alert.alert-info');
    if($msgAlert.length > 0)
    {
        setTimeout(function () {$msgAlert.remove()}, 8000)
    }

    $('.thumbnail').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });

});