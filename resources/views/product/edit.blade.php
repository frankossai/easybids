@extends('layouts.app')

@section('content')
@if (session('message'))
<div class="alert alert-success">
    {{ session('message') }}
</div>
@endif

<ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Products</li>
</ol>

<h1 class="page-header">Edit</h1>
<p><small></small></p>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-md-8">
        {!! Form::open(['url' => route('product.update', $product->id),  'method' => 'put']) !!}
        <div class="form-group">
            <label for="inputName">Name</label>
            {{Form::text('name', $product->name, ['class' => 'form-control', 'id' => 'inputName'])}}
        </div>
        <div class="form-group">
            <label for="inputPrice">Price</label>
            {{Form::text('price', $product->price, ['class' => 'form-control', 'id' => 'inputPrice'])}}
        </div>
        <div class="form-group">
            <label for="inputSku">Sku</label>
            {{Form::text('sku', $product->sku, ['class' => 'form-control', 'id' => 'inputSku'])}}
        </div>
        <div class="form-group">
            <label for="inputDescription">Description</label>
            {{Form::textarea('description', $product->description, ['class' => 'form-control', 'id' => 'inputDescription'])}}
        </div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-thumbs-up"></i>&nbsp;Update</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
