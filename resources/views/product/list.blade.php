<p class="text-right">
    <a href="/product/create" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add new product</a>
</p>
<hr>
@if(count($products))
    <div class="table-wrapper">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>SKU</th>
                    <th>Price</th>
                    <th class="text-right">Date created</th>
                    <th class="text-right">Last updated</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $key = 0; ?>
                @foreach ($products as $_product)
                    @php($key++)
                        <tr>
                            <td>{{ $key }}</td>
                            <td>{{ $_product->name }}</td>
                            <td>{{ $_product->sku }}</td>
                            <td>{{ $_product->price }}</td>
                            <td class="text-right">{{ $_product->created_at }}</td>
                            <td class="text-right">{{ $_product->updated_at }}</td>
                            <td class="text-right">
                                <a href="/product/view/{{ $_product->id }}"><i class="material-icons" data-toggle="tooltip" data-placement="top" title="View">pageview</i></a>
                                <a href="/product/edit/{{ $_product->id }}" ><i class="material-icons" data-toggle="tooltip" data-placement="top" title="Edit">mode_edit</i></a>
                                <a class="delete-item" data-name="{{ $_product->name }}" data-url="/product/delete/{{ $_product->id }}" href="#"><i class="material-icons" data-toggle="tooltip" data-placement="top" title="Delete">delete</i></a>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
@else
    <div class="well well-sm"><p class="text-center">No products found</p></div>
@endif