<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Easy Bids') }}</title>
    <link rel="stylesheet" href="{{ mix('/css/web.css') }}">
</head>
<body class="landing-page">
<div class="container">
    <div class="col-md-12">
        @if(count($products))
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $products->links() }}
                </div>
            </div>
            <div class="row">
                @foreach ($products as $_product)
                    <div class="col-sm-4 col-md-3">
                        <div class="thumbnail">
                            <div class="caption">
                                <h3>{{ $_product->name }}</h3>
                                <div class="desc-cont">
                                    {{ str_limit($_product->description, 100) }}
                                </div>
                                <span class="text-right action-buttons">
                                        <a href="#" class="btn btn-primary"><i
                                                    class="glyphicon glyphicon-circle-arrow-right"></i>&nbsp;view</a>
                                        <a href="#" class="btn btn-primary"><i class="glyphicon glyphicon-usd"></i>&nbsp;Bid</a>
                                    </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    {{ $products->links() }}
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="well well-sm"><p class="text-center">No products found</p></div>
                </div>
            </div>
        @endif
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
</body>
</html>
