<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $max = 2;
        for ($i= 0; $i <= $max; $i++) {
            DB::table('users')->insert([
                'name' => str_random(6),
                'email' => strtolower(str_random(6)).'@morecorp.co.za',
                'password' => bcrypt('test@123'),
                'created_at' => \Carbon\Carbon::now()
            ]);
        }

    }
    
}
