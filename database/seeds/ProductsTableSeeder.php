<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $max = 100;
        for ($i= 0; $i <= $max; $i++) {
            DB::table('products')->insert([
                'name' => str_random(4) . ' ' . str_random(6) . ' ' . str_random(4),
                'sku' => strtolower(str_random(6)),
                'price' => rand(10, 300),
                'description' => Lipsum::short()->text(2),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
